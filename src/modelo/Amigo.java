package modelo;

public class Amigo extends Pessoa {
	
	private String apelido;
	
	public Amigo(String nome, String eMail, String telefone, String apelido){
		super(nome,eMail,telefone);
		this.apelido = apelido;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String toString() {
		StringBuffer s = new StringBuffer();
		s.append("Nome: ");
		s.append(getNome());
		s.append("\ne-mail: ");
		s.append(geteMail());
		s.append("\nTelefone: ");
		s.append(getTelefone());
		s.append("\nApelido: ");
		s.append(apelido);
		return s.toString();
	}

}

