package modelo;

public class Contato extends Pessoa{

		private String localDeTrabalho;
		private String telefoneComercial;

		public Contato(String nome, String eMail, String telefone, String localDeTrabalho, String telefoneComercial){
			super(nome,eMail,telefone);
			this.localDeTrabalho = localDeTrabalho;
			this.telefoneComercial = telefoneComercial;
		}

		public String getLocalDeTrabalho() {
			return localDeTrabalho;
		}

		public void setLocalDeTrabalho(String localDeTrabalho) {
			this.localDeTrabalho = localDeTrabalho;
		}

		public String getTelefoneComercial() {
			return telefoneComercial;
		}

		public void setTelefoneComercial(String telefoneComercial) {
			this.telefoneComercial = telefoneComercial;
		}

		public String toString() {
			StringBuffer s = new StringBuffer();
			
			s.append("Nome: ");
			s.append(getNome());
			s.append("\ne-mail: ");
			s.append(geteMail());
			s.append("\nTelefone: ");
			s.append(getTelefone());
			s.append("\nLocal de Trabalho: ");
			s.append(localDeTrabalho);
			s.append("\nTelefone Comercial: ");
			s.append(telefoneComercial);
			return s.toString();
		}
		
		
}
