package visao;

import java.util.Scanner;

import modelo.Amigo;
import modelo.Contato;

public class Main {

	public static void main(String[] args) {
		String nome,eMail,telefone,localDeTrabalho,telefoneComercial,apelido;
		int opcao;
		Scanner ler = new Scanner(System.in);
		Amigo amigo;
		Contato contato;
		
		System.out.println("Insira o nome:");
		nome = ler.nextLine();
		System.out.println("Insira o email:");
		eMail = ler.nextLine();
		System.out.println("Insira o telefone:");
		telefone = ler.nextLine();
		
		System.out.println("Salvar essa pessoa como:\n 1-contato\n 2-amigo?");
		opcao = Integer.parseInt(ler.nextLine());
		//ler.nextLine();
		
		switch(opcao){
			case 1:
				System.out.println("Insira o local de trabalho:");
				localDeTrabalho = ler.nextLine();
				System.out.println("Insira o telefone comercial:");
				telefoneComercial = ler.nextLine();
				contato = new Contato(nome,eMail,telefone,localDeTrabalho,telefoneComercial);
				System.out.println(contato.toString());
				break;
			case 2:
				System.out.println("Insira o apelido:");
				apelido = ler.next();
				amigo = new Amigo(nome,eMail,telefone,apelido);
				System.out.println(amigo.toString());
				break;
		}
		System.out.println("\nUsuario cadastrado com sucesso!\n");
		
		
	}
}
